/**
 * Neste programa, o usuario digita as informa��es 
 * que s�o pedidas, e o programa calcula o IMC e informa
 * a classifica��o do usuario.
 * 
 *  @author Vitor Barbarulo
 */
import javax.swing.JOptionPane;

public class CalculoDeImc {

	public static void main(String[] args) {

		// variaveis
		int novamente;
		double imc, altura1, peso1;
		String altura, peso, ola;

		do {

			// mensagens e pede para escrever
			peso = JOptionPane.showInputDialog("Informe o peso (em kgs): ");

			if (peso == null) { // cancela
				break;
			}

			altura = JOptionPane.showInputDialog("Informe a altura (em metros): ");

			if (altura == null) { // cancela
				break;
			}

			altura1 = Double.parseDouble(altura);
			peso1 = Double.parseDouble(peso);

			// conta
			imc = peso1 / (altura1 * altura1);

			/*
			 * -> exibe o resultado da conta 
			 * -> mostra em uma mensagem na tela
			 */
			if (imc < 18.5)
				JOptionPane.showMessageDialog(null, "Abaixo do peso normal.");
			else if ((imc >= 18.5) && (imc < 25))
				JOptionPane.showMessageDialog(null, "Peso normal.");
			else if ((imc >= 25) && (imc < 30))
				JOptionPane.showMessageDialog(null, "Excesso de peso.");
			else if ((imc >= 30) && (imc < 35))
				JOptionPane.showMessageDialog(null, "Obesidade classe I.");
			else if ((imc >= 35) && (imc < 40))
				JOptionPane.showMessageDialog(null, "Obesidade classe II.");
			else
				JOptionPane.showMessageDialog(null, "Obesidade classe III.");

			// pergunta se quer calcular novamente
			novamente = JOptionPane.showConfirmDialog(null, "Deseja calcular novamente?", "Pergunta...",
					JOptionPane.YES_NO_OPTION);

		} while (novamente == 0);
	}

}