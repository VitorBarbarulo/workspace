package br.senai.sp.informatica.oop;
/**
 * 
 * @author Vitor Barbarulo
 *
 */
public class CachorroTestDrive {

	public static void main(String[] args) {

		Cachorro pitbull = new Cachorro();
		pitbull.raca = "Pitbull";
		pitbull.tamanho = 45;
		pitbull.nome = "Lucifer";
		pitbull.latir();

		Cachorro toto = new Cachorro();
		toto.raca = "Vira-lata";
		toto.tamanho = 32;
		toto.nome = "Jeremy";
		toto.latir();
	}

}
