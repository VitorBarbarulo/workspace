package br.senai.sp.informatica.oop;
/**
 * 
 * @author Vitor Barbarulo
 *
 */
public class Cachorro {

	// Atributos
	String raca;
	String nome;
	int tamanho;

	// Metodos
	void latir() {
		System.out.println("O " + this.raca + " latiu: \"Au Au Au !!!\"");

	}

}
