package br.senai.sp.informatica.oop;

public class Conta {

	// atributos
	String cliente;
	double saldo;

	// metodo
	void exibeSaldo() {
		System.out.println(cliente + " seu saldo � de R$: " + saldo);
	}

	void saca(double valor) {
		// - o valor do saldo
		saldo -= valor;
	}

	void deposita(double valor) {
		saldo += valor;
	}
	
	void tranferePara (Conta destino, double valor) {
		this.saca(valor);
		destino.deposita(valor);
	}

}
